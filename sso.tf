data "aws_caller_identity" "sso-current" {}

data "aws_ssoadmin_instances" "sso-instance" {
  provider = aws.ct-mgmt
}

data "aws_ssoadmin_permission_set" "sso-permision-set" {
  provider = aws.ct-mgmt
  instance_arn = tolist(data.aws_ssoadmin_instances.sso-instance.arns)[0]
  name         = "AWSAdministratorAccess"
}

data "aws_identitystore_group" "sso-user-group" {
  provider = aws.ct-mgmt
  identity_store_id = tolist(data.aws_ssoadmin_instances.sso-instance.identity_store_ids)[0]

  filter {
    attribute_path  = "DisplayName"
    attribute_value = "Admins"
  }
}

resource "aws_ssoadmin_account_assignment" "sso-account-assign" {
  provider = aws.ct-mgmt
  instance_arn       = data.aws_ssoadmin_permission_set.sso-permision-set.instance_arn
  permission_set_arn = data.aws_ssoadmin_permission_set.sso-permision-set.arn

  principal_id   = data.aws_identitystore_group.sso-user-group.group_id
  principal_type = "GROUP"

  target_id   = data.aws_caller_identity.sso-current.account_id
  target_type = "AWS_ACCOUNT"
}